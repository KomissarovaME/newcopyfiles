package copy_files;

import java.io.*;

public class WorkWithFiles {
    private String text = "";
    public WorkWithFiles(String inputFile) throws IOException {//ошибка ввода/вывода

        BufferedReader bufferedReader=new BufferedReader( new FileReader(inputFile));
        String line;
        while ((line=bufferedReader.readLine()) != null){
            text+=line+"\n";
        }
        bufferedReader.close();
    }

    /**
     * Метод записывающий в outputFile файл скопированные данные из inputFile файла.
     * @param outputFile
     */
    public void writeToFile(String outputFile){
        try {
            BufferedWriter bufferedWrite=new BufferedWriter( new FileWriter(outputFile));
            bufferedWrite.write(text);
            bufferedWrite.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}