package copy_files;

public class Main {

    public static void main(String[] args) {
        SequentialFileCopying.sequentialFileCopying();
        SequentialFileCopying.sequentialFileCopying();
        ParallelFileCopying.parallelFileCopying();
    }
}
