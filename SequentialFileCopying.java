package copy_files;

import java.io.IOException;
import java.util.Scanner;

public class SequentialFileCopying {
    private static Scanner scanner = new Scanner(System.in);

    public static void sequentialFileCopying() {
        System.out.println("Введите путь откуда взять файл: ");
        String inputFiles = scanner.nextLine();
        System.out.println("И куда его записать: ");
        String outputFiles = scanner.nextLine();
        rfrf(inputFiles, outputFiles);
    }

    private static void rfrf(String inputFiles, String outputFiles) {
        try {
            WorkWithFiles filesInputStream = new WorkWithFiles(inputFiles);
            System.out.println("Подождите, идёт копирование!");
            filesInputStream.writeToFile(outputFiles);
            System.out.println("Готово!Файл " + inputFiles + " скопирован.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
