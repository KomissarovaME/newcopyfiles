package copy_files;
import java.util.Scanner;

public class ParallelFileCopying{
    private static Scanner scanner = new Scanner(System.in);

    public static void parallelFileCopying() {
        System.out.println("Введите путь откуда взять первый файл: ");
        String inputFiles0 = scanner.nextLine();
        System.out.println("Введите путь откуда взять второй файл: ");
        String inputFiles1 = scanner.nextLine();
        System.out.println("Куда записать "+ inputFiles0 +": ");
        String outputFiles0 = scanner.nextLine();
        System.out.println("Куда записать "+ inputFiles1 +": ");
        String outputFiles1 = scanner.nextLine();

        Thread thread0 = new Thread(inputFiles0,outputFiles0);
        Thread thread1 = new Thread(inputFiles1,outputFiles1);

        thread0.run();
        thread1.start();
    }
}