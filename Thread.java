package copy_files;

import java.io.IOException;

public class Thread extends java.lang.Thread {
    String inputFiles;
    String outputFiles;
    public Thread(String inputFiles,String outputFiles){
        this.inputFiles=inputFiles;
        this.outputFiles=outputFiles;
    }

    public void run(){
        try {
            WorkWithFiles filesInputStream = new WorkWithFiles(inputFiles);
            System.out.println("Подождите, идёт копирование!");
            filesInputStream.writeToFile(outputFiles);
            System.out.println("Готово!Файл " + inputFiles + " скопирован в файл "+outputFiles);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}